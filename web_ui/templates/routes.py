from web_ui import app
from utils import fn
from flask import render_template, request, flash, redirect, url_for
from tinydb import TinyDB

# GLOBAL
PANORAMA = False

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', title='Home')


@app.route('/device_setup')
def device_setup():
    return render_template('device_setup.html', title='Device Setup')


@app.route('/save_device', methods=['POST'])
def save_device():

    # DB Stuff
    db = TinyDB(
            'utils/db.json',
            sort_keys=True,
            indent=4,
            separators=(',', ': ')
            )
    db.table('device').purge()
    panorama = request.form.get('panorama', False) == 'on'
    user = request.form['user']
    password = request.form['pass']
    host = request.form['host']
    url = f"https://{host}/api?"
    api_key = fn.keygen(user, password, url)
    if panorama:
        devices = fn.get_devices(url, api_key)
        db.table('device').insert({
            'host': host,
            'url': url,
            'api_key': api_key,
            'panorama': panorama,
            'devices': devices,
            })
        global PANORAMA
        PANORAMA = True
    else:
        zones = fn.get_zones(url, api_key)
        apps = fn.get_apps(url, api_key)
        db.table('device').insert(
            {
                'host': host,
                'url': url,
                'api_key': api_key,
                'panorama': panorama,
                'zones': zones,
                'apps': apps,
            }
        )

    flash('Device successfully configured!', 'success')
    return redirect(url_for('index'))


@app.route('/select_fw')
def select_fw():
    try:
        db = TinyDB(
            'utils/db.json',
            sort_keys=True,
            indent=4,
            separators=(',', ': ')
            )
        s = db.table('device').get(doc_id=1)

        if s['panorama']:
            return render_template(
                'select_fw.html',
                title='Select Firewall',
                panorama=s['panorama'],
                devices=s['devices'],
                )
        else:
            flash('Please configure a Panorama via "Configure Device" menu first!', 'danger')
            return redirect(url_for('index'))
    except BaseException:
        flash('Please configure a device first!', 'danger')
        return redirect(url_for('index'))


@app.route('/save_fw', methods=['POST'])
def save_fw():

    # DB Stuff
    db = TinyDB(
            'utils/db.json',
            sort_keys=True,
            indent=4,
            separators=(',', ': ')
            )
    s = db.table('device').get(doc_id=1)
    fw = request.form['fw']
    zones = fn.get_zones(s['url'], s['api_key'], fw)
    apps = fn.get_apps(s['url'], s['api_key'], fw)
    db.table('device').update(
        {
            'target_fw': fw,
            'zones': zones,
            'apps': apps,
            }
        )
    flash(f"Firewall with serial {fw} successfully selected!")
    return redirect(url_for('index'))


@app.route('/test_sec_params')
def test_sec_params():
    try:
        db = TinyDB(
            'utils/db.json',
            sort_keys=True,
            indent=4,
            separators=(',', ': ')
            )
        s = db.table('device').get(doc_id=1)
        return render_template(
            'test_sec_params.html',
            title='Test Parameters',
            zones=s['zones'],
            apps=s['apps'],
            panorama=PANORAMA
            )
    except BaseException as be:
        flash('Please configure a device first!', 'danger')
        return redirect(url_for('index'))


@app.route('/test_nat_params')
def test_nat_params():
    try:
        db = TinyDB(
            'utils/db.json',
            sort_keys=True,
            indent=4,
            separators=(',', ': ')
            )
        s = db.table('device').get(doc_id=1)

        return render_template(
            'test_nat_params.html',
            title='Test Parameters',
            zones=s['zones'],
            panorama=PANORAMA
            )
    except BaseException:
        flash('Please configure a device first!', 'danger')
        return redirect(url_for('index'))


@app.route('/run_test_sec', methods=['POST'])
def run_test_sec():

    test_dict = {
        'from': request.form['from'],
        'to': request.form['to'],
        'src': request.form['src'],
        'dst': request.form['dst'],
        'dport': request.form['dport'],
        'protocol': request.form['protocol'],
        'application': request.form['application'],
    }

    try:
        db = TinyDB(
            'utils/db.json',
            sort_keys=True,
            indent=4,
            separators=(',', ': ')
            )
        s = db.table('device').get(doc_id=1)
        if s['panorama']:
            serial = s['target_fw']
            rl = fn.test_sec_policy(
                    s['url'],
                    s['api_key'],
                    test_dict,
                    serial,
                    )
        else:
            rl = fn.test_sec_policy(s['url'], s['api_key'], test_dict)
        return render_template('run_test_sec.html', results=rl)
    except BaseException as be:
        flash('Please configure a device first!', 'danger')
        return redirect(url_for('index'))


@app.route('/run_test_nat', methods=['POST'])
def run_test_nat():

    test_dict = {
        'from': request.form['from'],
        'src': request.form['src'],
        'dst': request.form['dst'],
        'dport': request.form['dport'],
        'protocol': request.form['protocol'],
    }

    try:
        db = TinyDB(
            'utils/db.json',
            sort_keys=True,
            indent=4,
            separators=(',', ': ')
            )
        s = db.table('device').get(doc_id=1)
        if s['panorama']:
            serial = s['target_fw']
            rl = fn.test_nat_policy(
                    s['url'],
                    s['api_key'],
                    test_dict,
                    serial,
                    )
        else:
            rl = fn.test_nat_policy(s['url'], s['api_key'], test_dict)
        return render_template('run_test_nat.html', results=rl)
    except BaseException as be:
        flash('Please configure a device first!', 'danger')
        return redirect(url_for('index'))


@app.route('/select_cmd')
def select_cmd():

    try:
        db = TinyDB(
            'utils/db.json',
            sort_keys=True,
            indent=4,
            separators=(',', ': ')
            )
        s = db.table('commands').get(doc_id=1)

        return render_template(
            'select_cmd.html',
            title='Run OP Commands',
            commands=s,
            panorama=PANORAMA
            )
    except BaseException as be:
        flash(f"Unable to select command due to {be}!", 'danger')
        return redirect(url_for('index'))


@app.route('/run_cmd', methods=['POST'])
def run_cmd():

    try:
        db = TinyDB(
            'utils/db.json',
            sort_keys=True,
            indent=4,
            separators=(',', ': ')
            )
        s = db.table('device').get(doc_id=1)
        c = db.table('commands').get(doc_id=1)
        xpath = c[request.form['cmd']]
        if s['panorama']:
            serial = s['target_fw']
            results = fn.run_cmd(
                    s['url'],
                    s['api_key'],
                    xpath,
                    serial,
                    )
        else:
            results = fn.run_cmd(s['url'], s['api_key'], xpath)
        return render_template('run_cmd.html', results=results)
    except BaseException as be:
        flash(f"Unable to run command due to {be}!", 'danger')
        return redirect(url_for('index'))

@app.route('/static/<path:path>')
def send_static(path):
    """
    Serve static files from within GUnicorn. A better option
    than referencing CDN links that may be compromised and serve
    malicious JS at some point.
    """
    return send_from_directory('static', path)