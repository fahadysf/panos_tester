"""
Author: Nasir Bilal
Email: nbilal@paloaltonetworks.com
"""
import xmltodict
import requests
import re
import json

# Suppress warnings from requests library
requests.packages.urllib3.disable_warnings()


##############################################################
# FUNCTIONS
##############################################################
def api_request(url, values):
    """
    API driver function. The middle-man between the script and firewall.

    :param url:     string - URL to connect to ("https://192.168.1.1/api")
    :param values:  dictionary - the API command to be executed
    :return:        if successful, returns an XML response as a string
                    if unsuccessful, returns None
    """

    try:
        return requests.post(url, values, verify=False).content
    except requests.exceptions.ConnectionError:
        print("ERROR Connecting to {url}. Check IP address.".format(url=url))
        return None


def keygen(username: str, password: str, url: str) -> str:
    values = {'type': 'keygen', 'user': username, 'password': password}
    try:
        response = xmltodict.parse(api_request(url, values).decode())
        return response['response']['result']['key']
    except AttributeError:
        print(f'ERROR Obtaining API key from {url}...Check credentials.')
        return None

def get_devices(url: str, api_key: str) -> list:
    # BUILD DICTIONARY FOR "SHOW DEVICES CONNECTED" AND MAKE API CALL
    api_call_dict = {
        'type': 'op',
        'cmd': '<show><devices><connected/></devices></show>',
        'key': api_key,
    }
    try:
        response = api_request(url, api_call_dict).decode()
        devices = xmltodict.parse(response).get('response', {}).get(
            'result', {}).get('devices', {}).get('entry', {})
        if type(devices) == list:
            return devices
        else:
            return [devices]
    except BaseException as be:
        print(f"get_devices() failed due to {be}.")
        return None

def run_cmd(
        url: str, api_key: str, cmd_xml: str,
        serial: str=None,
        ) -> str:
    api_call_dict = {
        'key': api_key,
        'type': 'op',
        'cmd': cmd_xml,
    }
    if serial:
        api_call_dict['target'] = serial
    try:
        response = api_request(url, api_call_dict).decode()
        output_dict = xmltodict.parse(response).get('response', {}).get('result', {})
        parsed_response = json.dumps(output_dict, indent=4)
        return parsed_response
    except BaseException as e:
        print(f"FAILED DUE TO {e}.")
        return "None"


def get_zones(
        url: str, api_key: str,
        serial: str=None,
        ) -> list:

    xpath = ("/config/devices/entry[@name='localhost.localdomain']"
             "/vsys/entry[@name='vsys1']/zone")

    api_call_dict = {
        'key': api_key,
        'type': 'config',
        'action': 'get',
        'xpath': xpath,
    }
    if serial:
        api_call_dict['target'] = serial
    try:
        response = api_request(url, api_call_dict).decode()
        zone_section = xmltodict.parse(response)[
            'response']['result'].get('zone', {}).get('entry', {})
        zones = [z['@name'] for z in zone_section]
        return zones
    except BaseException as e:
        print(f"FAILED DUE TO {e}.")
        return []

def get_apps(
        url: str, api_key: str,
        serial: str=None,
        ) -> list:

    api_call_dict = {
        'key': api_key,
        'type': 'config',
        'action': 'get',
        'xpath': '/config/predefined/application',
    }
    if serial:
        api_call_dict['target'] = serial
    try:
        response = api_request(url, api_call_dict).decode()
        apps_section = xmltodict.parse(response)[
            'response']['result'].get('application', {}).get('entry', {})
        apps = [a['@name'] for a in apps_section]
        return apps
    except BaseException as e:
        print(f"FAILED DUE TO {e}.")
        return []

def test_sec_policy(
        url: str, api_key: str,
        test_dict: dict,
        serial: str=None,
        ) -> list:

    api_call_dict = {
        'key': api_key,
        'type': 'op',
        'cmd': '<test><security-policy-match><show-all>yes</show-all>'
                '<from>{from}</from><to>{to}</to><source>{src}</source>'
                '<destination>{dst}</destination><protocol>{protocol}'
                '</protocol><destination-port>{dport}</destination-port>'
                '<application>{application}</application>'
                '</security-policy-match></test>'.format(**test_dict),
    }
    if serial:
        api_call_dict['target'] = serial
    try:
        response = api_request(url, api_call_dict).decode()
        rule = xmltodict.parse(response)[
            'response'].get('result', {}).get('rules', {}).get('entry', {})
        if isinstance(rule, list):
            return rule
        elif isinstance(rule, dict):
            for k, v in rule.items():
                if isinstance(v, dict):
                    rule[k] = v['member']
            return [rule]
    except BaseException as e:
        print(f"FAILED DUE TO {e}.")
        return []


def test_nat_policy(
        url: str, api_key: str,
        test_dict: dict,
        serial: str=None,
        ) -> list:

    api_call_dict1 = {
        'key': api_key,
        'type': 'op',
        'cmd': '<test><nat-policy-match><from>{from}</from><source>{src}</source>'
               '<destination>{dst}</destination><protocol>{protocol}</protocol>'
               '<destination-port>{dport}</destination-port>'
               '</nat-policy-match></test>'.format(**test_dict),
    }
    api_call_dict2 = {
        'key': api_key,
        'type': 'config',
        'action': 'get',
        'xpath': "/config/panorama/vsys/entry[@name='vsys1']/post-rulebase/nat/rules",
    }
    if serial:
        api_call_dict1['target'] = serial
        api_call_dict2['target'] = serial
    try:
        response1 = api_request(url, api_call_dict1).decode()
        rule = xmltodict.parse(response1)[
            'response'].get('result', {}).get('rules', {}).get('entry', {})
        response2 = api_request(url, api_call_dict2).decode()
        rules = xmltodict.parse(response2)[
            'response'].get('result', {}).get('rules', {}).get('entry', {})
        matching_rule = [a for a in rules if a['@name'] == rule]
        return matching_rule
    except BaseException as e:
        print(f"FAILED DUE TO {e}.")
        return []
