FROM alpine AS builder
WORKDIR /cert
RUN apk update && \
    apk add --no-cache openssl && \
    rm -rf /var/cache/apk/* && \
    cd /cert && \
    openssl req -x509 -sha256 -nodes -days 730 \
    -subj "/OU=IT Department/CN=panos.tester" \
    -newkey rsa:2048 -keyout cert.key -out cert.crt


FROM python:alpine3.6
LABEL Nasir Bilal <bilalbox@gmail.com>

RUN adduser -D panos

ADD . / /home/panos/app/

WORKDIR /home/panos/app

COPY --from=builder /cert /home/panos/app

ENV FLASK_APP panos_tester_ui.py

RUN chown -R panos:panos ./; \
    cd /home/panos/app; \
    touch db.json; \
    chown panos:panos db.json;\
    chmod 600 db.json; \
    python -m venv venv; \
    source venv/bin/activate; \
    pip install -r requirements.txt; \
    venv/bin/pip install gunicorn

USER panos

CMD exec venv/bin/gunicorn -b :5000 --certfile=cert.crt --keyfile=cert.key panos_tester_ui:app

EXPOSE 5000

